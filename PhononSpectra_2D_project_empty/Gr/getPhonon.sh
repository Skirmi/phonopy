#!/bin/bash

export PHONOPY_PATH=$1
export MLIP_PHONOPY_PATH=$2
"$PHONOPY_PATH"phonopy -d --dim="8 8 1" -c POSCARunitcell
mv SPOSCAR POSCAR
num=$(find -type f -name "POSCAR-*" | wc -l)
echo $num
"$MLIP_PHONOPY_PATH"Main $num
cp POSCARunitcell POSCAR
"$PHONOPY_PATH"phonopy  -p -s band.conf
"$PHONOPY_PATH"phonopy-bandplot  --gnuplot band.yaml > B.txt

 grep " frequency" band.yaml  > freq.txt
  grep "group_velocity" band.yaml  > gv.txt
