#include "../mlip-2/dev_src/mtpr.h"

#include <sstream>
#include <iostream>

#ifdef MLIP_MPI
	#include <mpi.h>
#endif

using namespace std;

void ReadVaspPoscar(const string& fnm, Configuration& cfg)
{

	ifstream ifs(fnm);
	string line;
	getline(ifs, line);
	double factor;
	ifs >> factor;
	getline(ifs, line);
	for (int l = 0; l < 3; l++) {
		double l1, l2 ,l3;
		ifs >> l1 >> l2 >> l3;
		cfg.lattice[l][0] = l1*factor;
		cfg.lattice[l][1] = l2*factor;
		cfg.lattice[l][2] = l3*factor;
		getline(ifs, line);
	}

	getline(ifs, line);
	vector<string> vecstr1;
	string word1;
	stringstream s1(line);

	while (s1 >> word1) vecstr1.push_back(word1);

	getline(ifs, line);

	vector<string> vecstr2;
	string word2;
	stringstream s2(line);

	while (s2 >> word2) vecstr2.push_back(word2);

	int size = 0;

	vector<int> n_elems_of_type(vecstr2.size());
	for (int i = 0; i < vecstr2.size(); i++) {
		size += atoi(vecstr2[i].c_str());
		n_elems_of_type[i] = atoi(vecstr2[i].c_str());
	}		


	cfg.resize(size);

	while (line.substr(0, 6) != "Direct") 
		getline(ifs, line);

	int curr_type = 0;

	if (line.substr(0, 6) == "Direct") {
		int curr_size = n_elems_of_type[curr_type];
		for (int i = 0; i < cfg.size(); i++) {
			vector<double> x(3);
			string foo;
			ifs >> x[0] >> x[1] >> x[2];
			if (i == curr_size) {
				curr_type++;
				curr_size += n_elems_of_type[curr_type];
			}
                        cfg.type(i) = curr_type;
			for (int a = 0; a < 3; a++) {
				cfg.pos(i)[a] = 0.0;
				for (int b = 0; b < 3; b++) {
					cfg.pos(i)[a] += cfg.lattice[b][a]*x[b];
				}
			}
		}
	}
}

void SaveNextForcesSet(ifstream& ifs_yaml, ofstream& ofs, Configuration& cfg)
{
		string line2;
		while (line2.substr(0, 7) != "- atom:") 
			getline(ifs_yaml, line2);

		if (line2.substr(0, 7) == "- atom:") {
			vector<string> vecstr2;
			string word2;
			stringstream s2(line2);

			while (s2 >> word2) vecstr2.push_back(word2);	

			ofs << endl;
			ofs << vecstr2[vecstr2.size()-1].c_str() << endl;
		}
                

		while (line2.substr(0, 15) != "  displacement:") 
			getline(ifs_yaml, line2);

		if (line2.substr(0, 15) == "  displacement:") {
			char foo;
			double disp_x, disp_y, disp_z;
			ifs_yaml >> foo >> disp_x >> foo >> disp_y >> foo >> disp_z;
			ofs << disp_x << " " << disp_y << " " << disp_z << endl;
		}

		for (int i = 0; i < cfg.size(); i++)
			ofs << cfg.force(i)[0] << " " << cfg.force(i)[1] << " " << cfg.force(i)[2] << endl;
}

void MTPGenerateForceSetsFile(const char * pot_fnm, const int n_poscars, string& output)
{
		MLMTPR mtpr(pot_fnm);

		string file_yaml = "phonopy_disp.yaml";
		ifstream ifs_yaml(file_yaml);
		ofstream ofs(output);

		for (int k = 1; k <= n_poscars; k++) {

			string input = "POSCAR-";

			if (k < 10) input += "00" + to_string(k);
			else if (k >= 10 && k < 100) input += "0" + to_string(k);
			else input += to_string(k);

			Configuration cfg;

			ReadVaspPoscar(input, cfg);

			mtpr.CalcEFS(cfg);

			if (k == 1) {
				ofs << cfg.size() << endl;
				ofs << n_poscars << endl;
			}

			SaveNextForcesSet(ifs_yaml, ofs, cfg);

		}
}

void ReadBandFile(const string& bandfile, const int n_freqs, Array2D& frequency, Array1D& curr_length) {

	ifstream ifs(bandfile);

	string line;

	while (line.substr(0, 6) != "phonon")
		getline(ifs, line);

	char foo_char;
	string foo_string;

	int n_rows = 1;
	int n_spectrum_lines = 27;

	vector<Vector3> kpoints(12);
	double qposition[3];
	double qposition_prev[3];
	Array1D length_k(12);
	double length = 0;	

	for (int a = 0; a < 3; a++)
		qposition_prev[a] = 0;

	string kpointsfile = "band.conf";

	ifstream ifs_k(kpointsfile);

	getline(ifs_k, line);

	ifs_k >> foo_string >> foo_char;

	for (int k = 0; k < kpoints.size(); k++) 
		for (int a = 0; a < 3; a++) 
			ifs_k >> kpoints[k][a];

	while (!ifs.eof()) {

		curr_length.resize(n_rows);
		frequency.resize(n_rows, n_spectrum_lines);

		ifs >> foo_char >> foo_string >> foo_char >> qposition[0] >> foo_char >> qposition[1] >> foo_char >> qposition[2] >> foo_char >> foo_char;

		if (n_rows == 0) length += 0;
		else {
			double dist = 0;
			for (int a = 0; a < 3; a++) 
				dist += (qposition[a] - qposition_prev[a])*(qposition[a] - qposition_prev[a]);
			dist = sqrt(dist);
			length += dist;
			for (int a = 0; a < 3; a++) 
				qposition_prev[a] = qposition[a];
		}

		curr_length[n_rows-1] = length;

		getline(ifs, line);
		getline(ifs, line);

		for (int i = 0; i < n_spectrum_lines; i++) {
			getline(ifs, line);
			ifs >> foo_string >> frequency(n_rows-1, i) >> foo_char;
		}

		n_rows++;
		
	}	

	length = 0;
	length_k[0] = 0;

	for (int k = 0; k < kpoints.size()-1; k++) {
		double dist = 0;
		for (int a = 0; a < 3; a++) {
			dist += (kpoints[k+1][a] - kpoints[k][a])*(kpoints[k+1][a] - kpoints[k][a]);
		}
		dist = sqrt(dist);
		length += dist;
		length_k[k+1] = length;
	}
	
}

int main(int argc, char* argv[])
{

    int n_poscars; 
    if (argc != 2) {
        cout << "Please, enter number of POSCARs" << endl;
        return 0;
    }
    else {
        n_poscars = stoi(argv[1]);
    }

    const char * pot_fnm_mtp = "p.mtp";
    string output_mtp = "FORCE_SETS";

    MTPGenerateForceSetsFile(pot_fnm_mtp, n_poscars, output_mtp);

    return 0;
}
