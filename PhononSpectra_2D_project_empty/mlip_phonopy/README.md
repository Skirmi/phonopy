# MLIP_Phonopy
getPhonon.sh script allows to calculate phonon spectra using the MLIP code (namely, Moment Tensor Potential) and the Phonopy software. In order to calculate phonon spectrum:

1. Follow the instructions to install the Phonopy software: 

https://phonopy.github.io/phonopy/install.html

The version of the Phonopy code should be 2.2 or later, the version of python should be 3.6 or later. 

2. Download the MLIP code to the same folder as mlip_phonopy/:

git clone https://gitlab.com/ashapeev/mlip-2.git 

3. Install the MLIP code:

cd mlip-2/

./configure --no-mpi

make mlp

4. Download and install the latest version of Phonopy:

pip install phonopy

5. Install MLIP_Phonopy:

cd mlip_phonopy/

make

6. Go to the Example folder and run the getPhonon.sh script:

cd Example/

./getPhonon.sh path-to-phonopy path_to_Main_file_in_mlip_phonopy, e.g. ./getPhonon.sh /usr/local/bin/ ../
